---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Definition of ABM
Account-based marketing (ABM) is a business-to-business (B2B) strategy that focuses sales and marketing resources on target accounts within a specific market. Instead of broad-reaching marketing campaigns that touch the largest possible number of prospective customers, an ABM strategy focuses resources for the book of business on a defined set of named accounts. Then marketing focuses all of their resources, budget and campaigns against the very specific list of target accounts to directly support sales effort.


## Mission Statement

## Tools we use
### Demandbase
### TOPO

## Definitions
### Total addressable Market
### Ideal customer profile
### Target accounts
### Tiered Accounts

