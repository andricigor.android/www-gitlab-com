---
layout: handbook-page-toc
title: "Critical Security Release Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
# Critical Security Release

Moved to [release documentation for security releases](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/process.md#critical-security-releases).
