---
layout: handbook-page-toc
title: Learning Sessions
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Live Learning Schedule

1. The 2019 Live Learning schedule is as follows: 
   - December - Inclusion
1. The 2020 Live Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Live Learning Sessions
### 2019
1. October - [Communicating Effectively & Responsibly Through Text](/company/culture/all-remote/effective-communication/)

## Action Learning Schedule

1. The 2019 Action Learning schedule is as follows:
   - November - Annual Compensation Review
   - December - TBC
1. The 2020 Action Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC
