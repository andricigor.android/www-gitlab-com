features:
  primary:
    - name: "Incident Management now Viable"
      available_in: [ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/user/incident_management/index.html'
      video: 'https://www.youtube.com/embed/f4wNjjF9NhQ'
      reporter: sarahwaldner
      stage: monitor
      epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/1493'
      description: |
        Being on-call is stressful. Trying to dig through multiple tools to do a root cause analysis, update tickets in different places, and manage stakeholders during a firefight all at the same time can slow you down right at the time when you need to move fast. And yet, this is a typical workflow for anyone who has ever been responsible for keeping IT services available. We think your workflow should work for you not against you.  

        Incident Management with GitLab aims to alleviate those problems by giving you a single application with a streamlined workflow. We've been adding capabilities like customizable incidents, embeddable metrics from Prometheus and Grafana, ChatOps via Slack slash commands, and a REST endpoint that receives alerts from any source and automatically creates incidents. Today, we're excited to announce that GitLab Incident Management has moved from minimal to viable [maturity](/direction/maturity/). Rather than just an experience designed to validate customer need, we've now seen that businesses and organizations can benefit from Incident Management built-in to GitLab. At this point, we don't expect you will replace ServiceNow at a large enterprise, but for startups, SMBs, and smaller projects using GitLab for Incident Management simplifies your toolchain.  
        
        Eliminate middle-man ticketing systems that require access to simply update the tickets. By co-locating code and incidents in GitLab, you can reduce time spent responding to outages and spend more time innovating and building new things.
